
. libs/opsworks.sh

usage() {
  echo "
$0 -n "layer_name" [list|stop|start|delete] [all|online|start_failed|stopped]
$0 -n "layer_name" create [number]

or

layer_id=xxx $0 [list|stop|start|delete] [all|online|start_failed|stopped]
"
}

check_command_option() {
  [ "$layer_id" == "" ] && usage && exit 1
  # main
  case "$1" in
  list)
    $(show_instances_in_layer) | grep "$query"
  ;;
  stop)
    $(show_instances_in_layer) | grep -v "stopped" | grep "$query" | cut -f 1 | while read instance_id; do
      stop_instance_in_layer
    done
  ;;
  start)
    $(show_instances_in_layer) | grep -v "online" | grep "$query" | cut -f 1 | while read instance_id; do
      start_instance_in_layer
    done
  ;;
  delete)
    $(show_instances_in_layer) | grep -v "online" | grep "$query" | cut -f 1 | while read instance_id; do
      delete_instance_in_layer
    done
  ;;
  create)
    i=1
    if [ $# -eq 2 ]; then
      case "$2" in
      [1-9])
        i=$2;;
      [1-4][0-9])
        i=$2;;
      esac
    fi
    for n in $(seq 1 $i ); do
      create_instance_in_layer
    done
  ;;
  esac
}

while [ $# -gt 0 ]; do
  case "$1" in
list|stop|start|delete|create)
  check_command_option "$1" "$2"
  exit
  ;;
-n)
  name=$2
  [ "$name" == "" ] && usage && exit 1
  layer_id=$($(describe_layers_of_stack) | grep -P "\t$name$" | cut -f 1)
  #echo $layer_id
  shift 2
  ;;
*)
  usage; exit 1
;;
  esac
done
