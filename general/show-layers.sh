
. libs/opsworks.sh

usage() {
  echo "
stack_id=xxx $0
"
}

[ "$stack_id" == "" ] && usage && exit 1

if [ "$1" == "" ]; then
  $(describe_layers_of_stack)
else
  $(describe_layers_of_stack) | grep -P "\t$1$"
fi
