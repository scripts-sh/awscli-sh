
usage() {
  echo "$0 clean_cache"
  echo "$0 deploy"
  exit 1
}

stack_id="dab023dc-8235-4500-9544-3a52d0cc77aa"
instances_id="21d68f74-e27e-47df-9764-1d747bca0fc8"
app_id="b42e511d-8f2c-4493-b937-12e42319428e"

run_command_clean_cache() {
  json='{ "Name": "execute_recipes", "Args": { "recipes": [ "nba-ct-deployment::clean_cache" ] } }'
aws opsworks --region us-east-1 create-deployment \
--stack-id $stack_id \
--instance-ids $instances_id \
--command "$json"
}

run_command_deploy() {
   json='{ "Name": "deploy" }'
aws opsworks --region us-east-1 create-deployment \
--stack-id $stack_id \
--instance-ids $instances_id \
--app-id $app_id \
--command "$json"

}

case "$1" in
clean_cache)
  run_command_clean_cache
;;
deploy)
  run_command_deploy
;;
*)
usage
;;
esac
