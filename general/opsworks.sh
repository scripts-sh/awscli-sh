
. libs/opsworks.sh

#list
#list layer_name

case "$1" in
list)
  if [ "$2" == "" ]; then
    describe_stacks
  else
    stack_id=$(describe_stacks | grep "$2" | cut -f 1)
    describe_layers_of_stack | bash
  fi
;;
*)
  echo "no command"
;;
esac
