#!/bin/bash

script_name=$(readlink -f $0)
cwd=$(dirname $script_name)
. ${cwd}/libs/ec2.sh

case "$1" in
ami-*)
  image_ids="$1"
  $(show_amis)
;;
*)
  $(show_all_my_ami)
;;
esac
