awscli
------

### Setup Guide

* IAM role : opsworks:CreateDeployment

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1416287913000",
      "Effect": "Allow",
      "Action": [
        "opsworks:CreateDeployment"
      ],
      "Resource": [
        "arn:aws:opsworks:*:*:stack/87cae8a7-5131-4f0d-9681-9e4899901de0/"
      ]
    }
  ]
}
```

* IAM role: ec2 create-image

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1415788507000",
      "Effect": "Allow",
      "Action": [
        "ec2:CreateImage"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
```


* IAM role: opsworks:CreateInstance, opsworks:RebootInstance, opsworks:StartInstance, opsworks:StopInstance

Handle Layers: list, create, start, stop, delete

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1416296546000",
      "Effect": "Allow",
      "Action": [
        "opsworks:CreateInstance",
        "opsworks:RebootInstance",
        "opsworks:StartInstance",
        "opsworks:StopInstance"
      ],
      "Resource": [
        "arn:aws:opsworks:*:*:stack/55916b69-f7d7-4de6-b5d7-1f1b8b4ce541/"
      ]
    }
  ]
}
```

