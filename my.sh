#!/bin/bash

script_name=$(readlink -f $0)
cwd=$(dirname $script_name)
. ${cwd}/libs/ec2.sh
. ${cwd}/libs/opsworks.sh

init_vars() {
  stack_name=$(show_my_stack_name)
  stack_id=$(describe_stacks | grep -P "$stack_name$" | cut -f 1)
}

set_auto_type() {
  if [[ "${1}" =~ ^(timer|load)$ ]] ; then
    scale_type=$1
  fi
}

handle_layers() {
  layer_name=$1
  [ "$layer_name" == "" ] && describe_layers_of_stack && return
  query=$3
  case "$2" in
  list)
    layer_id=$(get_layer_id)
    check_layer_id
    $(show_instances_in_layer)
  ;;
  create)
    layer_id=$(get_layer_id)
    check_layer_id
    num=$3
    set_auto_type $4
    i=1
    case "$num" in
    [1-9])
      i=$num;;
    [1-4][0-9])
      i=$num;;
    esac
    for n in $(seq 1 $i ); do
      create_instance_in_layer
    done
  ;;
  stop)
    layer_id=$(get_layer_id)
    check_layer_id
    $(show_instances_in_layer) | grep -v -E "time|load" | grep "online" | grep "$query" | cut -f 1 | while read instance_id; do
      stop_instance_in_layer
    done
  ;;
  start)
    layer_id=$(get_layer_id)
    check_layer_id
    $(show_instances_in_layer) | grep -v -E "time|load" | grep "stopped" | grep "$query" | cut -f 1 | while read instance_id; do
      start_instance_in_layer
    done
  ;;
  delete)
    layer_id=$(get_layer_id)
    check_layer_id
    $(show_instances_in_layer) | grep "stopped" | grep "$query" | cut -f 1 | while read instance_id; do
      delete_instance_in_layer
    done
  ;;
  *)
   echo "[usage] $0 layers layer_name list|create"; exit 1
  ;; 
  esac
}

case "$1" in
layers)
  init_vars
  shift 1
  handle_layers $@
;;
info)
  init_vars
  echo "stack: $stack_id $stack_name"
;;
add_tag)
  init_vars
  add_tags_to_instances_in_stack
;;
*)
  echo "[usage] $0 layers|info|add_tag"
;;
esac
