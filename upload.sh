ip="54.169.132.205"
ssh -t ubuntu@${ip} "sudo mkdir /opt/cli; sudo chown -R ubuntu /opt/cli"
scp -r libs ubuntu@${ip}:/opt/cli/
scp {my.sh,run.sh,show-subnets.sh,show-ami.sh} ubuntu@${ip}:/opt/cli/
scp {sample*.config,readme.md} ubuntu@${ip}:/opt/cli/
