#!/bin/bash

script_name=$(readlink -f $0)
cwd=$(dirname $script_name)
if [[ "$stack_id" == "" || "$ami_id" == "" || "$instance_type" == "" ]]; then
  [ ! -f ${cwd}/config ] && echo "[error] can't find config" && exit 1 || . ${cwd}/config
fi

###################
# Stack and Layer
###################

describe_stacks() {
  aws opsworks --region us-east-1 describe-stacks --query Stacks[*].[StackId,Name] --output text 
}

describe_layers_of_stack() {
  aws opsworks --region us-east-1 describe-layers --stack-id $stack_id --query Layers[*].[LayerId,Name] --output text | tee /tmp/.cli.layers.cache
  #echo "aws opsworks --region us-east-1 describe-layers --stack-id $stack_id --query Layers[*].[LayerId,Name] --output text"
}

###################
# Instance in Layer
###################

show_instances_in_layer() {
  #aws opsworks --region us-east-1 describe-instances --layer-id $layer_id --query Instances[*]
  echo "aws opsworks --region us-east-1 describe-instances --layer-id $layer_id --query Instances[*].[InstanceId,Hostname,AvailabilityZone,InstanceType,Status,AutoScalingType,PublicIp] --output text"
}

generate_subnet() {
  local i=$(($RANDOM%2))
  if  [ "$i" == "0" ]; then
    subnet_id=$(echo $subnets| cut -d " " -f 1 | cut -d , -f 1)
    subnet_zone=$(echo $subnets | cut -d " " -f 1 | cut -d , -f 2)
  else
    subnet_id=$(echo $subnets| cut -d " " -f 2 | cut -d , -f 1)
    subnet_zone=$(echo $subnets | cut -d " " -f 2 | cut -d , -f 2)
  fi
}

create_instance_in_layer() {
  # ref function: set_auto_type
  if [[ "${scale_type}" =~ ^(timer|load)$ ]] ; then
    auto_type="--auto-scaling-type ${scale_type}"
  else
    auto_type=""
  fi
  generate_subnet
  echo "aws opsworks create-instance \
--stack-id $stack_id --layer-ids $layer_id \
--instance-type $instance_type --region us-east-1 \
--ami-id $ami_id --os Custom \
--subnet-id $subnet_id \
$auto_type \
--availability-zone $subnet_zone"
}

###################
# Instance
###################


start_instance_in_layer() {
  echo "aws opsworks --region us-east-1 start-instance --instance-id $instance_id"
}

stop_instance_in_layer() {
  echo "aws opsworks --region us-east-1 stop-instance --instance-id $instance_id"
}

delete_instance_in_layer() {
  echo "aws opsworks --region us-east-1 delete-instance --instance-id $instance_id"
}


###################
# Get Layer Info
###################

check_layer_id() {
  if [ "$layer_id" == "" ]; then
    echo "[error] can't find layer_id of $layer_name"
    exit 1
  fi
}

get_layer_id_from_cache() {
    cat /tmp/.cli.layers.cache | grep -P "\t$layer_name$" | cut -f 1
}

get_layer_id() {
    if [ -f /tmp/.cli.layers.cache ]; then
        get_layer_id_from_cache
    else
        describe_layers_of_stack | grep -P "\t$layer_name$" | cut -f 1
    fi
}

# for create instance in layer
set_auto_type() {
  if [[ "${1}" =~ ^(timer|load)$ ]] ; then
    scale_type=$1
  fi
}
