#!/bin/bash

script_name=$(readlink -f $0)
cwd=$(dirname $script_name)
if [ "$owner_id" == "" ]; then
  [ ! -f ${cwd}/config ] && echo "[error] can't find config" && exit 1 || . ${cwd}/config
fi

show_all_my_ami() {
  echo "aws ec2 describe-images --region ap-southeast-1 --owners $owner_id --query Images[*].[ImageId,Name,VirtualizationType,State] --output text"
}

show_amis() {
  echo "aws ec2 describe-images --region ap-southeast-1 --image-ids $image_ids --region ap-southeast-1 --query Images[*].[ImageId,Name,VirtualizationType,State] --output text"
}

show_my_stack_name() {
  instance_id=$(curl http://169.254.169.254/latest/meta-data/instance-id 2> /dev/null)  
  #aws ec2 --region ap-southeast-1 describe-instances --instance-ids $instance_id --query "Reservations[*].Instances[*].Tags[*].Key[opsworks:stack]"
  #aws ec2 --region ap-southeast-1 describe-tags "Name=key,Values=$instance_id" "Name=value,Values=$instance_id"
  aws ec2 --region ap-southeast-1 describe-tags  --filters "Name=resource-id,Values=$instance_id" "Name=key,Values=opsworks:stack" \
  --query "Tags[*].Value" --output text
}

add_tags_to_instances_in_stack() {

  resouces_id=$(aws ec2 describe-tags  --region ap-southeast-1 --filters "Name=value,Values=$stack_name" --output text | awk '{ print $3 }')
  echo $resouces_id

  aws ec2 create-tags  --region ap-southeast-1 \
--resources $resouces_id --tags $tags

}
