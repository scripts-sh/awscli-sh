
mac=$(curl http://169.254.169.254/latest/meta-data/network/interfaces/macs/ 2> /dev/null)

vpc_id=$(curl http://169.254.169.254/latest/meta-data/network/interfaces/macs/${mac}vpc-id 2> /dev/null)

echo $vpc_id

aws ec2 describe-subnets --filters "Name=vpc-id,Values=${vpc_id}" --region ap-southeast-1 --query "Subnets[*].[Tags[0].Value,SubnetId,AvailabilityZone,CidrBlock]" --output text
