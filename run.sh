#!/bin/bash

script_name=$(readlink -f $0)
cwd=$(dirname $script_name)
. ${cwd}/libs/ec2.sh
. ${cwd}/libs/opsworks.sh

usage() {
    echo "[usage] $script_name deploy layer_name                 # sample: deploy dochk-webview"
    echo "[usage] $script_name recipe recipe_name layer_name     # sample: recipe nxweb::restart_php dochk-webview"
}

init_vars() {
    stack_name=$(show_my_stack_name)
    stack_id=$(describe_stacks | grep -P "$stack_name$" | cut -f 1)
}

check_layer_info() {
    local layer_info=$1
    local layer_name=$1
    local layer_id=$(get_layer_id)
    check_layer_id
    instance_ids="$($(show_instances_in_layer) | grep online | cut -f 1)"
    if [[ "$instance_ids" == "" ]]; then
        echo "[error] no running instance in this layer."; usage ; exit 1
    fi
}

run_recipe() {
    local recipe_name=$1
    json='{ "Name": "execute_recipes", "Args": { "recipes": [ "'${recipe_name}'" ] } }'
#    echo $json
    echo aws opsworks --region us-east-1 create-deployment \
        --stack-id $stack_id \
        --instance-ids $(echo $instance_ids) \
        --command \'$json\'

}

run_deploy() {
    json='{ "Name": "deploy" }'
#    echo $json
    echo aws opsworks --region us-east-1 create-deployment \
        --stack-id $stack_id \
        --instance-ids $(echo $instance_ids) \
        --app-id $app_id \
        --command \'$json\'
}


# options : deploy [layer_name], recipe [cookbook::recipe] [layer_name]

case "$1" in
    deploy)
        init_vars
        check_layer_info "$2"
        run_deploy
        ;;
    recipe)
        init_vars
        check_layer_info "$3"
        run_recipe "$2"
        layer_info=$2
        ;;
    cache)
        layer_name=$2
        echo $(get_layer_id_from_cache)
        ;;
    *)
        usage
        exit 1
        ;;
esac
